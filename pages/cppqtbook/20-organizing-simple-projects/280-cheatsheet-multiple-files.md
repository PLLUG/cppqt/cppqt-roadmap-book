---
title:  Cheatsheet.  Програми,  які  складаються  з  кількох  файлів.
sidebar:  cppqtbook_sidebar
permalink:  cheatsheet-multiple-files.html
folder:  cppqtbook/20-organizing-simple-projects
---


##  Створення  окремого  файлу  програми  та  заголовного  файлу

  1.  Створіть  два  файли  з  розширеннями  .cpp  та  .h.  Дайте  їм  однакову  назву,  яка  буде  описувати  їх  вміст.
  2. Додайте до заголовного файлу include guards. Назвою  для  макроса  може  бути,  наприклад,  назва  заголовного  файлу,  головне  щоб  макрос  був  достатньо  унікальний  і  не  конфліктував  своїм  ім'ям  з  іншими.
```cpp
#ifndef  /*макрос*/
#define  /*макрос*/
//  Вміст  заголовного  файлу
#endif
```
  3.  Розмістіть визначеня функцій у cpp-файлі, а оголошення цих функцій у h-файлі.


##  Створення  статичної  бібліотеки
 1. Створіть .cpp  та  .h файли для бібліотеки, додайте програмний код бібліотеки. Переконайтеся, що усі необхідні оголошення, є у заголовному файлі біліотеки.
 2. Скомпілюйте файл коду.
 ```bash
 clang++  -c  -o  <назва файлу>.o  <назва файлу>.cpp
 ```
  * параметр  `-c`  означає,  що  треба  створити  об'єктний  файл
 3. Створіть статичну бібліотеку з об'єктного файлу
 ```bash
llvm-ar  rc  lib<назва файлу>.a  ./<назва файлу>.o
```
  *  `llvm-ar`  -  програма,  яка  створює  бібліотеку
  *  `rc`  -  параметри,  `r`  -  переписати,  якщо  існує,  `c`  -  створити  нову  бібліотеку

##  Компіляція  проекту,  який  складається  з  багатьох  файлів
1. Скомпілюйте кожен файл окремо
Скомпілюйте файл коду.
 ```bash
 clang++  -c  -o  <назва файлу>.o  <назва файлу>.cpp
 ```
  * параметр  `-c`  означає,  що  треба  створити  об'єктний  файл
  * додатково, під час компіляції кожного з файлів, додайте розміщення заголоних файлів, які підключено у даному файлі (якщо необхідно!) з допомогою опції `-I<шлях  до  теки>`.  Опція може повторюватися кілька разів, якщо необхідно додати кілька тек з заголовними файлами.

 2. Створіть виконуваний файл з окремих об'єктних файлів:
```bash
clang++  <список об'єктних файлів розділений пробілом> -o  <назва виконуваного файлу>
```
Додатково:
  *  параметр  `-L<шлях  до  теки>`  задає  шлях  до  теки,  де  лежать  бібліотеки  які  підключають  до  програми.  Таких  параметірв  може  бути  кілька  -  якщо  потрібно  вказати  кілька  шляхів.  Зверніть  увагу,  що  коли  ми  задаємо  цей  параметр,  то  пробіл  між  `-L`  та  шляхом  відсутній.
  *  `-l<назва бібліотеки>`  -  підключаємо  бібліотеку  `lib<назва бібліотеки>.a`.  Коли  вказуємо  ім'я  бібліотеки  -  то  маємо  на  увазі,  що  задане  ім'я  буде  мати  префікс  `lib`  на  початку  і  завершуватиметься  розширенням  `.a`
