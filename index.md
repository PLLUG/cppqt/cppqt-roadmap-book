---
title: C++\Qt Roadmap Book
keywords: homepage
permalink: index.html
sidebar: cppqtbook_sidebar
---

[**PLLUG C++/Qt Roadmap**](http://pllug.org.ua/qt-roadmap/) - це можливість послідовно крок за кроком освоїти мову [*С++*](https://isocpp.org/) та кросплатформний інструментарій розробки [*Qt5*](https://www.qt.io/).

Тут зібрані навчальні матеріали до C++/Qt Roadmap. Основною ціллю цієї книги є послідовне освітлення багатьох тем, які обговорювалися на зібраннях PLLUG C++/Qt Roadmap. Надалі ця книга може слугувати основним джерелом інформації для самостійного вивчення, а також зручним довідником.
